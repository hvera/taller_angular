import { Component, OnInit } from '@angular/core';
import { Oficina } from './oficina';
import { OficinasService } from './oficinas.service';

@Component({
  selector: 'app-oficina-admin',
  templateUrl: './oficina.component.html',
  styleUrls: ['./oficina.component.css']
})
export class OficinaAdminComponent implements OnInit {

  oficina: Oficina = new Oficina();
  listaOficinas: Oficina[] = [];
  isAdmin = true;


  constructor(private oficinaService: OficinasService) { }

  ngOnInit() {
    this.listaOficinas = this.oficinaService.getOficinas();
  }

  guardarOficina() {
    this.oficinaService.guardarOficina(this.oficina);
    this.oficina = new Oficina();
  }

}
