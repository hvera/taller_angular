import { Component } from '@angular/core';

declare var jQuery: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Course';

  irArriba() {
    jQuery('body, html').animate({
      scrollTop: '0px'
    }, 300);
  }
}
