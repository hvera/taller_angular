import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AppRoutingGuard implements CanActivate {

    canActivate() {
        // return true;
        const answer = prompt('Ingrese codigo super secreto ;)');
        if (answer === 'ANGULAR') {
            return true;
        } else {
            console.log('Acceso denegado, la respuesta fue: ' + answer);
            return false;
        }
    }
}
