import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Persona } from './persona';
import { PersonasService } from './personas.service';
import { SharedService } from '../shared/shared.service';
import { Pais } from '../shared/pais';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit, AfterViewInit, OnDestroy {

  persona: Persona = new Persona();
  listaPersonas: Persona[] = [];
  listaPaises: Pais[] = [];

  constructor(
    private servicioPersonas: PersonasService,
    private servicioShared: SharedService
  ) { }

  ngOnInit() {
    console.log('ejecutando codigo onInit');
    this.listaPersonas = this.servicioPersonas.getPersonas();
    this.servicioShared.getAllCountries().subscribe(paises => {
      this.listaPaises = paises;

    },
    err => {
      console.log(err);
    });

  }

  ngAfterViewInit() {
    console.log('ejecutando codigo afterViewInit');
  }

  ngOnDestroy() {
    console.log('ejecutando codigo onDestroy');
  }

  guardarPersona() {
    // this.listaPersonas.push(this.persona);
    this.servicioPersonas.guardarPersona(this.persona);
    this.persona = new Persona();
    console.log(this.listaPersonas);
  }

}
