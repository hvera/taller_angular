import { Pais } from '../shared/pais';

export class Persona {
    nombre: String;
    apellido: String;
    pais: Pais;
    fechaNacimiento: Date;
}
